import java.util.*;

class Playground {
    public static void main(String[ ] args) {
        System.out.println("Hello Java");

        System.out.println("================= Intro Programming =================");

        // Bulb Interface
        Bulb b = new Bulb();
        System.out.println("bulb is on return: " + b.isOnFun());
        b.turnOn();
        System.out.println("bulb is on return: " + b.isOnFun());

        // Shape Abstract
        double width = 2, length = 3;
        Shape rectangle = new Rectangle(width, length);
        System.out.println("Rectangle width: " + width + " and length: " + length + "\nArea: " + rectangle.area() + "\nPerimeter: " + rectangle.perimeter());
        
        double radius = 10;
        Shape circle = new Circle(radius);
        System.out.println("Circle radius: " + radius + "\nArea: " + rectangle.area() + "\nPerimeter: " + rectangle.perimeter());
        
        // Enum
        BulbEnum benum = new BulbEnum();
        benum.size = BulbEnum.BulbSize.LARGE;
        System.out.println("Bulb Size: " + benum.size);

        System.out.println("================== Binary Search ================");
        int[] arr_num = {1,2,3,4,5,6,7,8,9,10};
        BinarySearch binsearch = new BinarySearch();
        System.out.println(binsearch.BinarySearchIteration(arr_num, arr_num.length, 8));
        System.out.println(binsearch.BinarySearchRecursive(arr_num, arr_num.length, 8, 0, 10));

        System.out.println("========== Largest Sum Contiguous Sub Array ============");
        int[] arr_sum_contiguous = {1, -2, 3, 4, -4, 6, -14, 100, 2, 5};
        System.out.println(maxSubArraySum(arr_sum_contiguous));
        
        System.out.println("========== Factorial ==========");
        System.out.println(factorial(10));
        System.out.println(factorialIteration(10));
        printInt1(120);
        System.out.println("");
        printInt2(8, 2);
        System.out.println("");

        System.out.println("============ Tower of Hanoi ===========");
        towerOfHanoi(3, 'A', 'C', 'B');

        System.out.println("============ Fibonacci =============");
        System.out.println(fibonacci(3));
        
        
        System.out.println("============ ADT =============");
        ArrayList<Integer> al = new ArrayList<Integer>();
        
        al.add(1); // add 1 to the end of the list
        al.add(2); // add 2 to the end of the list
        al.add(3); // add 3 to the end of the list
        al.add(4); // add 4 to the end of the list
        System.out.println("Contents of Array: " + al); // array is converted to
        // string and printed to screen
        al.add(2,9); // 9 is added to 2nd index.
        al.add(5,9); // 9 is added to 5th index.
        System.out.println("Contents of Array: " + al);
        System.out.println("Array Size: " + al.size()); // array size printed
        System.out.println("Array IsEmpty: " + al.isEmpty());
        al.remove(al.size() -1); // last element of array is removed.
        System.out.println("Array Size: " + al.size());
        System.out.println("Array IsEmpty: " + al.isEmpty());
        al.removeAll(al); // all the elements of array are removed.
        System.out.println("Array IsEmpty: " + al.isEmpty());
        
        System.out.println("============== ADT - Linked Lists =================");
        LinkedList<Integer> ll = new LinkedList<Integer>();
        ll.addFirst(8); // 8 is added to the list
        ll.addLast(9); // 9 is added to last of the list.
        ll.addFirst(7); // 7 is added to first of the list.
        ll.addLast(20); // 20 is added to last of the list
        ll.addFirst(2); // 2 is added to first of list.
        ll.addLast(22); // 22 is added to last of the list.
        System.out.println("Contents of Linked List: " + ll);
        ll.removeFirst();
        ll.removeLast();
        System.out.println("Contents of Linked List: " + ll);

        System.out.println("============== ADT - Stack ==============");

    }

    /**
     * In the coming chapters, we will use ArrayDeque object. We'll not be making DequeStack wrapper class.
     */
    public class DequeStack<T> {
        private ArrayDeque<T> deque = new ArrayDeque<T>();

        // Add a new item to the top of the stack
        public void push(T obj) {
            deque.push(obj);
        }

        // Remove an element from the top of the stack and return its value
        public T pop() {
            return deque.pop();
        }

        // Returns the value of the element at the top of the stack
        public T top() {
            return deque.peekLast();
        }

        // Returns the number of elements in the stack
        public int size() {
            return deque.size();
        }

        // Determines whether the stack is empty.
        public boolean isEmpty() {
            return deque.isEmpty();
        }
    }

    /**
     * Abstract Data Type (ADT)
     * 
     * Array (storing data contiguously)
     * - good if we know the number of elements to be stored
     * - have dirrect access
     * 
     * Linked List (not storing data contiguously, use links that point to the next elements)
     * - slower then arrays because no direct access to linked list elements.
     * - useful data structure when we do not know the number of elements to be stored ahead of time.
     * - can have many variants, linear, circular, doubly, doubly circular.
     * API of linked list
     * 1. insert(k): adds k to the start of the list
     * 2. Delete(): delete element at the start of the list
     * 3. PrintList(): display all the elements of the list. Start with the first elements and then follow the references. This operation will take O(N) time.
     * 4. Find(k): find the position of element with value k. NOTE: binary search doesnt work on linked lists.
     * 5. FindKth(k): find element at position k. O(N)
     * 6. IsEmpty(): 
     * 
     * Stack
     * 1. Push(): Adds a new item to the top of the stack
     * 2. Pop(): Remove an element from the top of the stack and return its value
     * 3. Top(): Returns the value of the element at the top of the stack
     * 4. Size(): Returns the number of elements in the stack
     * 5. IsEmpty(): determines whether the stack is empty. It returns 1 if the stack is empty or return 0.
     * Note: All the above Stack operations are implemented in O(1) Time Complexity
     */
    
    /**
     * Recursive Function
     * Important properties of recursive algorithm:
     * 1. should have termination condition
     * 2. should change its state, and move towards the termination condition.
     * 3. should call itself
     * 
     * Note: the speed of a recursive program is slower because of stack overheads (when you call functions/method, it would keep your next instruction in the caller function to the stack).
     * If the same task can be achieved by iterative solutions (loops), then we should prefer iterative solution in order to avoid stack overhead.
     * Note: without termination condition, the recursive method may run forever and will finally consume all the stack memory.
     */
    public static int factorial(int i) {
        return i <= 1 ? 1 : i * factorial(i - 1);
    }
    
    public static int factorialIteration(int i) {
        int total = 1;
        while (i > 1) { // avoid another one loop by distinguish >= into > 1 and set total into 1
            total = total * i;
            i--;
        }
        return total;
    }
    
    public static void printInt1(int number) {
        char digit = (char)(number % 10 + '0');
        number = number / 10;
        if (number != 0) {
            printInt1(number/10);
        }
        System.out.print("" + digit);
    }
    
    public static void printInt2(int number, final int base) {
        String conversion = "0123456789ABCDEF";
        
        char digit = (char)(number % base);
        number = number / base;
        if (number != 0) {
            printInt2(number, base);
        }
        System.out.print("" + conversion.charAt(digit));
    }
    
    /**
     * Tower of Hanoi
     * ada N keping dan 3 tower (A, B, C),
     * ingin pindah dari A ke C dengan bantuan tower B
     * step2nya secara garis besar seperti ini
     * 1. Pindahin N-1 ke B
     * 2. Pindahin yang paling besar di A ke C
     * 3. Pindahin N-1 ke C
     * 
     * Jika input n, maka total step atau function yang dipanggil sebesar (2^n) - 1
     */
    public static void towerOfHanoi(int num, char from, char to, char tmp) {
        if (num < 1) {
            return;
        }
        
        // 1. Move N-1 into tmp tower
        towerOfHanoi(num - 1, from, tmp, to);
        
        // 2. Move the largest disk from and into dest tower
        System.out.println("Move " + num + " from " + from + " to " + to);
        
        // 3. Move N-1 into dest tower
        towerOfHanoi(num - 1, from, to, tmp);
    }
    
    public static int fibonacci(int n) {
        return n <= 1 ? n : fibonacci(n - 1) + fibonacci(n - 2);
    }
    
    /////////////////////////////////////////////////////////
    public static int maxSubArraySum(int[] arr) {
        int length = arr.length;
        
        int largest_sum = 0;
        
        for (int idx = 0; idx < length; idx++) {
            if (idx == 0) {
                continue;
            }
            
            int curr_val = arr[idx];
            int prev_val = arr[idx - 1];
            
            int sum_val = curr_val + prev_val;
            if (sum_val > largest_sum) {
                largest_sum = sum_val;
            }
        }
        return largest_sum;
    }
}

// Binary Search
class BinarySearch {
    public int BinarySearchIteration(int arr[], int size, int value) {
        int mid;
        int low = 0;
        int high = size - 1;
        
        while (low <= high) {
            // use low + (high - low) / 2 instead of (high + low) / 2 to avoid overflow.
            // https://ai.googleblog.com/2006/06/extra-extra-read-all-about-it-nearly.html
            mid = low + (high - low) / 2; // get the middle position from low and highest possible position
            
            if (arr[mid] == value) {
                return mid;
            } else if (arr[mid] < value) {
                low = mid + 1;
            } else if (arr[mid] > value) {
                high = mid - 1;
            }
        }
        
        return -1;
    }

    public int BinarySearchRecursive(int arr[], int size, int value, int low, int high) {
        
        int new_low = low;
        int new_high = high;
        // use low + (high - low) / 2 instead of (high + low) / 2 to avoid overflow.
        // https://ai.googleblog.com/2006/06/extra-extra-read-all-about-it-nearly.html
        int mid = low + (new_high - new_low) / 2;
        
        // validate high and low
        if (high < low) {
            return -1;
        }
        
        if (arr[mid] == value) {
            return mid;
        } else if (arr[mid] < value) {
            new_low = mid + 1;
            return BinarySearchRecursive(arr, size, value, new_low, new_high);
        } else if (arr[mid] > value) {
            new_high = mid - 1;
            return BinarySearchRecursive(arr, size, value, new_low, new_high);
        } else {
            return -1;
        }
    }
}


// Enum, restrict a variable to have one of the few predefined values.
class BulbEnum {
    // Enums
    enum BulbSize{ SMALL, MEDIUM, LARGE }
    BulbSize size;
}


// /**
//  * Nested class on Linked List and Tree.
//  * Both the linked list and tree have nodes to store new element.
//  * Both the linked list and tree have their nodes different, so it is best to declare their
//  * corresponding nodes class inside their own class to prevent name conflict and increase encapsulation.
//  */
// class LinkedList {
//     private static class Node {
//         private int value;
//         private Node next;
//         // Nested Class Node other fields and methods.
//     }
    
//     private Node head;
//     // Outer Class LinkedList other fields and methods.
// }

// class Tree {
//     private static class Node {
//         private int value;
//         private Node lChild;
//         private Node rChild;
//         // Nested Class Node other fields and methods.
//     }
    
//     private Node root;
//     // Outer Class Tree other fields and methods.
// }


////////////////////////// Shape Abstract

abstract class Shape {
    // Abstract Method
    public abstract double area();
    
    // Astract Method
    public abstract double perimeter();
}

class Circle extends Shape {
    private double radius;
    
    // constructor
    public Circle() { // jika tidak ada parameter, panggil lagi dengan parameter 1 by default untuk set radius defaultnya
        this(1);
    }
    
    public Circle(double r) {
        radius = r;
    }
    
    public void setRadius(double r) {
        radius = r;
    }
    
    @Override
    public double area() {
        // Area = phi * r * r
        return Math.PI * Math.pow(radius, 2);
    }
    
    @Override
    public double perimeter() {
        // Perimeter = 2 phi r
        return 2 * Math.PI * radius;
    }
}

class Rectangle extends Shape {
    private double width, length;
    
    public Rectangle() {
        this(1, 1);
    }
    
    public Rectangle(double w, double l) {
        width = w;
        length = l;
    }
    
    public void setWidth(double w) {
        width = w;
    }
    
    public void setLength(double l) {
        length = l;
    }
    
    @Override
    public double area() {
        // Area = width * length
        return width * length;
    }
    
    @Override
    public double perimeter() {
        // Perimeter = 2 (width + length)
        return 2 * (width + length);
    }
}


///////////////////// BULB
interface BulbInterface {
    public void turnOn();
    public void turnOff();
    public boolean isOnFun();
}

class Bulb implements BulbInterface {
    private boolean isOn=false;

    @Override
    public void turnOn() {
        isOn = true;
    }

    @Override
    public void turnOff() {
        isOn = false;
    }

    @Override
    public boolean isOnFun() {
        return isOn;
    }
}
