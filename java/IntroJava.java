class IntroJava {

    public static void main(String[ ] args) {

        System.out.println("Hello Java");



        // Bulb Interface

        Bulb b = new Bulb();

        System.out.println("bulb is on return: " + b.isOnFun());

        b.turnOn();

        System.out.println("bulb is on return: " + b.isOnFun());

        

        // Shape Abstract

        double width = 2, length = 3;

        Shape rectangle = new Rectangle(width, length);

        System.out.println("Rectangle width: " + width + " and length: " + length + "\nArea: " + rectangle.area() + "\nPerimeter: " + rectangle.perimeter());

        

        double radius = 10;

        Shape circle = new Circle(radius);

        System.out.println("Circle radius: " + radius + "\nArea: " + rectangle.area() + "\nPerimeter: " + rectangle.perimeter());

        

        // Enum

        BulbEnum benum = new BulbEnum();

        benum.size = BulbEnum.BulbSize.LARGE;

        System.out.println("Bulb Size: " + benum.size);

    }

}



// Enum, restrict a variable to have one of the few predefined values.

class BulbEnum {

    // Enums

    enum BulbSize{ SMALL, MEDIUM, LARGE }

    BulbSize size;

}





/**

 * Nested class on Linked List and Tree.

 * Both the linked list and tree have nodes to store new element.

 * Both the linked list and tree have their nodes different, so it is best to declare their

 * corresponding nodes class inside their own class to prevent name conflict and increase encapsulation.

 */

class LinkedList {

    private static class Node {

        private int value;

        private Node next;

        // Nested Class Node other fields and methods.

    }

    

    private Node head;

    // Outer Class LinkedList other fields and methods.

}



class Tree {

    private static class Node {

        private int value;

        private Node lChild;

        private Node rChild;

        // Nested Class Node other fields and methods.

    }

    

    private Node root;

    // Outer Class Tree other fields and methods.

}





////////////////////////// Shape Abstract



abstract class Shape {

    // Abstract Method

    public abstract double area();

    

    // Astract Method

    public abstract double perimeter();

}



class Circle extends Shape {

    private double radius;

    

    // constructor

    public Circle() { // jika tidak ada parameter, panggil lagi dengan parameter 1 by default untuk set radius defaultnya

        this(1);

    }

    

    public Circle(double r) {

        radius = r;

    }

    

    public void setRadius(double r) {

        radius = r;

    }

    

    @Override

    public double area() {

        // Area = phi * r * r

        return Math.PI * Math.pow(radius, 2);

    }

    

    @Override

    public double perimeter() {

        // Perimeter = 2 phi r

        return 2 * Math.PI * radius;

    }

}



class Rectangle extends Shape {

    private double width, length;

    

    public Rectangle() {

        this(1, 1);

    }

    

    public Rectangle(double w, double l) {

        width = w;

        length = l;

    }

    

    public void setWidth(double w) {

        width = w;

    }

    

    public void setLength(double l) {

        length = l;

    }

    

    @Override

    public double area() {

        // Area = width * length

        return width * length;

    }

    

    @Override

    public double perimeter() {

        // Perimeter = 2 (width + length)

        return 2 * (width + length);

    }

}





///////////////////// BULB

interface BulbInterface {

    public void turnOn();

    public void turnOff();

    public boolean isOnFun();

}



class Bulb implements BulbInterface {

    private boolean isOn=false;



    @Override

    public void turnOn() {

        isOn = true;

    }



    @Override

    public void turnOff() {

        isOn = false;

    }



    @Override

    public boolean isOnFun() {

        return isOn;

    }

}


